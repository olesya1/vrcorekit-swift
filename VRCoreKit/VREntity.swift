//
//  VREntity.swift
//  VRCoreKit
//
//  Created by Alice on 07/06/16.
//  Copyright © 2016 Voodoo Mobile. All rights reserved.
//

import UIKit
import Bond
import ReactiveKit

open class VREntity: VRBaseEntity {
    fileprivate typealias BaseEntity = VRBaseEntity
    fileprivate var allProperties: Dictionary<String, Any.Type> = [:]
    fileprivate var nestedProperties: Dictionary<String, Any.Type> = [:]
    
    public override init() {
        super.init()
        self.constructorHelper()
    }
    
    required public init(dictionary: [String : Any]) {
        super.init()
        self.constructorHelper()
        self.fromDictionary(dictionary)
    }
    
    public init(userDefaultsKey: String) {
        super.init()
        let dictionary = UserDefaults.standard.dictionary(forKey: userDefaultsKey)
        if dictionary == nil {
            return
        }
        self.constructorHelper()
        self.fromDictionary(dictionary!)
    }
    
    func constructorHelper() {
        allProperties = self.getAllPropertiesInfo()!
        nestedProperties = self.getNestedPropertiesInfo()!
    }
    
    public func serializeToUserDefaultsForKey(_ key: String) {
        UserDefaults.standard.set(self.toDictionary(), forKey: key)
        UserDefaults.standard.synchronize()
    }
    
    public func fromDictionary(_ dictionary: [String: Any]) {
        for (name, mirrorValue) in Mirror(reflecting: self).getChildrenRecursive() {
            
            let value = createValueFromDictValue(dictionary[name], forKey: name)
            if value is NSNull || value == nil {
                continue
            }
            
            if let storableValue = mirrorValue as? VRStorableToDictionary {
                if(!(dictionary[name] is NSNull) && dictionary[name] != nil) {
                    storableValue.fromDictionaryValue(value)
                }
                
                if let validatableValue = storableValue as? VRValidatableProtocol {
                    if !validatableValue.validate() {
                        print("value \(name) in struct " + type(of: self).description() + " didn't pass validation")
                    }
                }
            }
                
            else {
                super.setValue(value, forKey: name)
            }
        }
    }
    
    public func createValueFromDictValue(_ value: Any?, forKey key: String) -> Any? {
        
        if value is NSNull || value == nil {
            return nil
        }
        
        if allProperties[key] == nil { // no corresponding property in class
            return nil
        }
        
        if let _ = allProperties[key] as? NSDate.Type { // Date
            return Date.utcDateFromString(value as? String)
        }
        
        if nestedProperties[key] == nil { // value is of integral type
            return value
        }
        
        let klass = nestedProperties[key]! // VREnum or VREntity
        
        if let enumClass = klass as? VREnum.Type { // VREnum
            let enumValue = enumClass.init()
            enumValue.name = (value as? String)!
            return enumValue
        }
        
        let modelClass = klass as? VREntity.Type // VREntity
        if modelClass == nil {
            return nil
        }
        
        if let array = value as? Array<AnyObject> { // value is an array of VREntities
            var entityArray = Array<BaseEntity>()
            for dict in array {
                let nested = modelClass!.init(dictionary: (dict as? [String : AnyObject])!)
                entityArray.append(nested)
            }
            
            return entityArray
        }
        
        // value is a nested VREntity
        return modelClass!.init(dictionary: (value as? [String : AnyObject])!)
    }
    
    public func toDictionary() -> [String: Any] {
        var dict = [String: Any]()
        for (name, mirrorValue) in Mirror(reflecting: self).getChildrenRecursive()
        {
            if let validatableValue = mirrorValue as? VRValidatableProtocol {
                if !validatableValue.validate() {
                    continue
                }
            }
            
            // if value is wrapped in reactive property - get it out. otherwise use the value as is. in both cases proceed as usual afterwards
            var value = mirrorValue
            if let observableValue = value as? VRStorableToDictionary { // reactive cocoa bindable
                let dictValue = observableValue.toDictionaryValue()
                if dictValue is NSNull {
                    continue
                }
                value = dictValue!
            }
                
            else if self.value(forKey: name) == nil { // WARN!! app fails if you try extracting observable<> with valurForKey method
                continue
            }
            
            if let dateValue = value as? Date {
                dict[name] = dateValue.utcDateString()
            }
            else if let enumValue = value as? VREnum {
                dict[name] = enumValue.name
            }
            else if let entityValue = value as? VREntity { // nested VREntity
                dict[name] = entityValue.toDictionary()
            }
            else if let entityArray = value as? Array<BaseEntity> { // array of VREntities
                var dictArray = Array<[String: Any]>()
                for entity in entityArray {
                    dictArray.append((entity as! VREntity).toDictionary())
                }
                dict[name] = dictArray
            }
            else { // property of integral type
                dict[name] = value as Any
            }
        }
        return dict
    }
}
