//
//  VRStylable.swift
//  CoOpCube
//
//  Created by Alice on 08/07/16.
//  Copyright © 2016 Voodoo Mobile. All rights reserved.
//

import UIKit

public enum FieldState : Int {
    case empty, filled, editing, invalid, ready
}

protocol VRStylable {
    var didChangeQuery: ((String) -> ())? { get set }
    var didClearText: (() -> ())? { get set }
    var didBeginEditing: (() -> ())? { get set }
    var didEndEditing: (() -> ())? { get set }
    var shouldReturn: (() -> ())? { get set }
    var shouldChangeCharactersInRange: ((String) -> Bool)? { get set }
    
    var fieldState: FieldState { get set }
}
