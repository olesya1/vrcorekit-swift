//
//  VRTabsSwitcher.swift
//  CoOpCube
//
//  Created by Alice on 03/06/16.
//  Copyright © 2016 Voodoo Mobile. All rights reserved.
//

import UIKit

open class VRTabsSwitcher: VRButtonsSwitcher
{
    @IBOutlet public var contentView: VRPlaceholderView?
    @IBOutlet public var controllers: [UIViewController]?

    override public var selectedIndex: Int {
        willSet {
            if contentView === nil {
                fatalError("Please assign a view to the contentView variable")
            }
            contentView!.controller = controllers![newValue]
            
            super.selectedIndex = newValue
        }
    }
}
