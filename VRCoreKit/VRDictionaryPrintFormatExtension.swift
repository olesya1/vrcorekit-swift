//
//  VRDictionaryPrintFormatExtension.swift
//  VRCoreKit
//
//  Created by Alice on 16/02/2017.
//  Copyright © 2017 Voodoo Mobile. All rights reserved.
//

import UIKit

extension Dictionary {
    
    func toFormattedString() -> String {
        return toFormattedStringHelper(self, indent:0)
    }
    
    func toFormattedStringHelper(_ dictionary: Dictionary, indent: Int) -> String
    {
        var result = String()
        result += createIndentation(indent) + "{\n"
        
        for (key, value) in dictionary {
            
            let indentString = createIndentation(indent+1)
            result += indentString + "\"\(key)\":"
            
            if let dictValue = value as? Dictionary {
                result += "\n" + toFormattedStringHelper(dictValue, indent: indent + 1)
            }
                
            else if let arrayValue = value as? Array<Any> {
                if arrayValue.count == 0 {
                    result += "[]"
                }
                else {
                    result += " [\n"
                    for arrayElement in arrayValue {
                        if let dictValue = arrayElement as? Dictionary {
                            result += toFormattedStringHelper(dictValue, indent: indent + 2)
                        } else {
                            result += (arrayElement as AnyObject).description
                        }
                        result += ",\n"
                    }
                    removeLastCommaInString(&result)
                    result += indentString + "]"
                }
            }
                
            else if let stringValue = value as? String {
                result += " \"\(printVersion(stringValue))\""
            }
                
            else if value is NSNull {
                result += " null"
            }
                
            else {
                result += " " + (value as AnyObject).description
            }
            
            result += ",\n"
        }
        
        removeLastCommaInString(&result)
        result += createIndentation(indent) + "}"
        return result
    }
}

func createIndentation(_ indent: Int) -> String {
    return String(repeating: "\t", count: indent)
}

func printVersion(_ string: String) -> String {
    let maxCharCount = 200
    var result = string
    if string.characters.count > maxCharCount {
        let beginning = (string as NSString).substring(to: maxCharCount)
        result = beginning + "<...>"
    }
    return result
}

func removeLastCommaInString(_ string: inout String) {
    let index = string.index(string.endIndex, offsetBy: -2)
    string.remove(at: index)
}
