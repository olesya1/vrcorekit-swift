//
//  ViewController.swift
//  CoOpCubeAdmin
//
//  Created by Alice on 02/08/16.
//  Copyright © 2016 Voodoo Mobile. All rights reserved.
//

import UIKit
import SDWebImage

extension UIImageView {
    public func setImageWithUrlString(_ string: String?, placeholderImage: UIImage! = nil) {
        if string != nil {
            self.sd_setImage(with: URL(string: string!) as URL!, placeholderImage: placeholderImage)
        }
        else {
            self.image = placeholderImage
        }
    }
}
