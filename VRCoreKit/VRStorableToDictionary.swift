//
//  VRStorableToDictionary
//  VRCoreKit
//
//  Created by Alice on 16/01/2017.
//  Copyright © 2017 Voodoo Mobile. All rights reserved.
//

import UIKit

public protocol VRStorableToDictionary {
    func toDictionaryValue() -> Any?
    func fromDictionaryValue(_ value: Any?)
}
