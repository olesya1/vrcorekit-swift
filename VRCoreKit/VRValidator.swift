//
//  VRValidator.swift
//  VRCoreKit
//
//  Created by Alice on 14/02/2017.
//  Copyright © 2017 Voodoo Mobile. All rights reserved.
//

import UIKit

public protocol VRValidator {
    func validate(_ value: Any?) -> Bool
}

open class Required: VRValidator {
    
    public init() {}
    
    public func validate(_ value: Any?) -> Bool {
        
        if value == nil {
            return false
        }
        
        if let stringValue = value as! String? {
            return stringValue.characters.count > 0
        }
        return true
    }
}

open class Email: VRValidator {
    
    public init() {}
    
    public func validate(_ value: Any?) -> Bool {
        
        if let stringValue = value as! String? {
            return VRValidatorEmail().validate(stringValue)
        }
        return false
    }
}
