//
//  ReactivePropertyExtension.swift
//  VRCoreKit
//
//  Created by Alice on 16/01/2017.
//  Copyright © 2017 Voodoo Mobile. All rights reserved.
//

import ReactiveKit

extension Property: VRStorableToDictionary {

    public func toDictionaryValue() -> Any? {
        return self.value
    }
    
    public func fromDictionaryValue(_ value: Any?) {
        self.value = value as! Value
    }

}
